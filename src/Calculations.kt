package com.company


object Calculations {
    fun positionGeometricCenter(points: Array<Point2D?>): Point2D {
        var gcX = 0.0
        var gcY = 0.0

        for (point in points) {
            gcX += point!!.x
            gcY += point.y
        }

        return Point2D(gcX / points.size, gcY / points.size)

    }

    fun positionCenterOfMass(materialPoint: Array<MaterialPoint2D?>): Point2D {
        var comX = 0.0
        var comY = 0.0
        var comMass = 0.0

        for (point in materialPoint) {
            comX += point!!.x * point.mass
            comY += point.y * point.mass
            comMass += point.mass
        }

        return MaterialPoint2D(comX / comMass, comY / comMass, comMass)
    }
}
